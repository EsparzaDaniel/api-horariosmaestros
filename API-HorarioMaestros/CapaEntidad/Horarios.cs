﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
   public class Horarios
    {
        public int id { get; set; }

        public string dia { get; set; }

        public string rangohoras { get; set; }

        public string ubicacion { get; set; }

        public int fk_maestro { get; set; }
        public string nombre { get; set; }
    }
}
