﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_HorarioMaestros.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PrincipalController : Controller
    {
        [HttpGet("AlmacenarUsuario")]
        public string Almacenar(string Usuario, string Nombre, string Correo, string Password)
        {
            var Almacena = new StorageSQL();
            bool resultado = Almacena.AlmacenarUsuario(Usuario,Nombre, Correo, Password);
            if (resultado == true)
                return "Usuario registrado";
            else
                return "Usuario no registrado";
        }

        [HttpGet("BuscarUsuario")]
        public JsonResult BuscarUsuario(int Id)
        {
            var Consulta = new StorageSQL();
            var Lista = Consulta.BuscarUsuario(Id);
            return Json(Lista);
        }

        [HttpGet("EliminarUsuario")]
        public string EliminarUsuario(int Codigo)
        {
            var Almacena = new StorageSQL();
            bool resultado = Almacena.EliminarUsuario(Codigo);
            if (resultado == true)
                return "Usuario " + Codigo + "Eliminado";
            else
                return "Error, no se ha eliminado a usuario " + Codigo;
        }

        [HttpGet("ActualizarUsuario")]
        public string ActualizarUsuario(int Id, string Usuario, string Nombre, string Correo, string Password)
        {
            var Almacena = new StorageSQL();
            bool resultado = Almacena.EditarUsuario(Id, Usuario, Nombre, Correo, Password);
            if (resultado == true)
                return "Usuario actualizado";
            else
                return "Usuario no actualizado";
        }

        [HttpGet("ConsultarUsuarios")]
        public JsonResult ConsultarUsuarios()
        {
            var Consulta = new StorageSQL();
            var Lista = Consulta.MostrarTodosUsuarios();
            return Json(Lista);
        }
        [HttpGet("AlmacenarMaestro")]
        public string AlmacenarMaestro(string Nombre, string Academia)
        {
            var Almacena = new StorageSQL();
            bool resultado = Almacena.AlmacenarMaestros(Nombre, Academia);
            if (resultado == true)
                return "Maestro registrado";
            else
                return "Maestro no registrado";
        }

        [HttpGet("BuscarMaestro")]
        public JsonResult BuscarMaestro()
        {
            var Consulta = new StorageSQL();
            var Lista = Consulta.ConsultarMaestros();
            return Json(Lista);
        }

        [HttpGet("BuscarMaestroCompleto")]
        public JsonResult BuscarMaestroCompleto(string nombre)
        {
            var Consulta = new StorageSQL();
            var Lista = Consulta.ConsultarMaestrosCompleto(nombre);
            return Json(Lista);
        }

        [HttpGet("EliminarMaestro")]
        public string EliminarMaestro(int Id)
        {
            var Almacena = new StorageSQL();
            bool resultado = Almacena.EliminarMaestros(Id);
            if (resultado == true)
                return "Maestro " + Id + " Eliminado";
            else
                return "Error, no se ha eliminado al maestro " + Id;
        }

        [HttpGet("ActualizarMaestro")]
        public string ActualizarMaestro(int Id, string Nombre, string Academia)
        {
            var Almacena = new StorageSQL();
            bool resultado = Almacena.EditarMaestros(Id, Nombre, Academia);
            if (resultado == true)
                return "Maestro actualizado";
            else
                return "Maestro no actualizado";
        }

       
        [HttpGet("AlmacenarHorario")]
        public string AlmacenarHorario(string Dia, string RangoHora, string Ubicacion, int Maestro)
        {
            var Almacena = new StorageSQL();
            bool resultado = Almacena.AlmacenarHorario(Dia, RangoHora, Ubicacion, Maestro);
            if (resultado == true)
                return "Horario registrado";
            else
                return "Horario no registrado";
        }

        [HttpGet("BuscarHorario")]
        public JsonResult BuscarHorario(string nombre)
        {
            var Consulta = new StorageSQL();
            var Lista = Consulta.ListarHorarios(nombre);
            return Json(Lista);
        }

        [HttpGet("EliminarHorario")]
        public string EliminarHorario(int Id)
        {
            var Almacena = new StorageSQL();
            bool resultado = Almacena.EliminarHorarios(Id);
            if (resultado == true)
                return "Horario " + Id + " Eliminado";
            else
                return "Error, no se ha eliminado el horario " + Id;
        }

        [HttpGet("ActualizarHorario")]
        public string ActualizarHorario(int Id, string Dia, string RangoHora, string Ubicacion, int Maestro)
        {
            var Almacena = new StorageSQL();
            bool resultado = Almacena.EditarHorario(Id ,Dia, RangoHora, Ubicacion, Maestro);
            if (resultado == true)
                return "Horario actualizado";
            else
                return "Horario no actualizado";
        }

    }
}
