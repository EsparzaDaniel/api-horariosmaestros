﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CapaEntidad;

namespace API_HorarioMaestros
{
    public class StorageSQL
    {
        List<Alumnos> ListaUsuarios;
        List<Maestros> ListaMaestros;
        List<Horarios> ListaHorarios;
        Alumnos alumnos;
        Maestros maestros;

        public bool AlmacenarUsuario(string Usuario, string Nombre, string Correo, string Password)
        {
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var querry = new SqlCommand("EXEC GuardarUsuarios '" + Usuario + "','" + Nombre + "','" + Correo + "','" + Password + "'", connect);
            try
            {
                connect.Open();
                querry.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception)
            {
                connect.Close();
                return false;
            }
        }
        public bool EditarUsuario(int Id, string Usuario, string Nombre, string Correo, string Password)
        {
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var querry = new SqlCommand("EXEC EditarUsuarios '" + Id + "','" + Usuario + "','" + Nombre + "','" + Correo + "','" + Password + "'", connect);
            try
            {
                connect.Open();
                querry.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception)
            {
                connect.Close();
                return false;
            }
        }

        public bool EliminarUsuario(int Id)
        {
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var querry = new SqlCommand("EXEC EliminarUsuarios '" + Id + "'", connect);
            try
            {
                connect.Open();
                querry.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception)
            {
                connect.Close();
                return false;
            }
        }
        public List<Alumnos> BuscarUsuario(int Id)
        {
            var dt = new DataTable();
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var cmd = new SqlCommand("Exec ConsultarUsuarios '" + Id + "'", connect);
            try
            {
                ListaUsuarios = new List<Alumnos>();
                connect.Open();
                var da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                connect.Close();
                Alumnos a = new Alumnos(); ;
                a.id = int.Parse(dt.Rows[0]["id_usuario"].ToString());
                a.usuario = dt.Rows[0]["usuario"].ToString();
                a.nombre = dt.Rows[0]["nombre"].ToString();
                a.correo = dt.Rows[0]["correo"].ToString();
                a.password = dt.Rows[0]["password"].ToString();
                ListaUsuarios.Add(a);
                return ListaUsuarios;
            }
            catch (Exception)
            {
                connect.Close();
                return ListaUsuarios;
            }
        }
        public List<Alumnos> MostrarTodosUsuarios()
        {
            var dt = new DataTable();
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var cmd = new SqlCommand("Exec ConsultarTodosUsuarios", connect);
            try
            {
                ListaUsuarios = new List<Alumnos>();
                connect.Open();
                var da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                connect.Close();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    alumnos = new Alumnos(); 
                    alumnos.id = int.Parse(dt.Rows[i]["id_usuario"].ToString());
                    alumnos.usuario = dt.Rows[i]["usuario"].ToString();
                    alumnos.nombre = dt.Rows[i]["nombre"].ToString();
                    alumnos.correo = dt.Rows[i]["correo"].ToString();
                    alumnos.password = dt.Rows[i]["password"].ToString();
                    ListaUsuarios.Add(alumnos);
                }
                return ListaUsuarios;
            }
            catch (Exception)
            {

                connect.Close();
                return ListaUsuarios;
            }
        }

        public bool AlmacenarMaestros(string Nombre, string Academia)
        {
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var query = new SqlCommand("EXEC GuardarMaestros '" + Nombre + "','" + Academia + "'", connect);
            try
            {
                connect.Open();
                query.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception)
            {
                connect.Close();
                return false;
            }
        }

        public bool EditarMaestros(int Id, string Nombre, string Academia)
        {
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var query = new SqlCommand("EXEC EditarMaestros '" + Id + "','" + Nombre + "','" + Academia + "'", connect);
            try
            {
                connect.Open();
                query.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception)
            {
                connect.Close();
                return false;
            }
        }

        public bool EliminarMaestros(int Id)
        {
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var query = new SqlCommand("EXEC EliminarMaestros '" + Id + "'", connect);
            try
            {
                connect.Open();
                query.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception)
            {
                connect.Close();
                return false;
            }
        }

        public List<Maestros> ConsultarMaestros()
        {
            var dt = new DataTable();
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var cmd = new SqlCommand("Exec ConsultarMaestro", connect);
            try
            {
                ListaMaestros = new List<Maestros>();
                connect.Open();
                var da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                connect.Close();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    maestros = new Maestros();
                    maestros.id = int.Parse(dt.Rows[i]["id_maestro"].ToString());
                    maestros.nombre = dt.Rows[i]["nombre"].ToString();
                    ListaMaestros.Add(maestros);
                }
                return ListaMaestros;
            }
            catch (Exception)
            {
                connect.Close();
                return ListaMaestros;
            }
        }

        public List<Maestros> ConsultarMaestrosCompleto(string nombre)
        {
            var dt = new DataTable();
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var cmd = new SqlCommand("Exec BuscarMaestro '" + nombre + "'", connect);
            try
            {
                ListaMaestros = new List<Maestros>();
                connect.Open();
                var da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                connect.Close();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    maestros = new Maestros();
                    maestros.id = int.Parse(dt.Rows[i]["id_maestro"].ToString());
                    maestros.nombre = dt.Rows[i]["nombre"].ToString();
                    maestros.academia = dt.Rows[i]["academia"].ToString();
                    ListaMaestros.Add(maestros);
                }
                return ListaMaestros;
            }
            catch (Exception)
            {
                connect.Close();
                return ListaMaestros;
            }
        }

        public bool AlmacenarHorario(string Dia, string RangoHoras, string Ubicacion, int Maestro)
        {
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var querry = new SqlCommand("EXEC GuardarHorarios '" + Dia + "','" + RangoHoras + "','" + Ubicacion + "','" + Maestro + "'", connect);
            try
            {
                connect.Open();
                querry.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception)
            {
                connect.Close();
                return false;
            }
        }
        public bool EditarHorario(int Id, string Dia, string RangoHoras, string Ubicacion, int Maestro)
        {
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var querry = new SqlCommand("EXEC EditarHorarios '" + Id + "','" + Dia + "','" + RangoHoras + "','" + Ubicacion + "','" + Maestro + "'", connect);
            try
            {
                connect.Open();
                querry.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception ex)
            {
                connect.Close();
                return false;
            }
        }

        public bool EliminarHorarios(int Id)
        {
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var querry = new SqlCommand("EXEC EliminarHorarios '" + Id + "'", connect);
            try
            {
                connect.Open();
                querry.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (Exception)
            {
                connect.Close();
                return false;
            }
        }

        public List<Horarios> ListarHorarios(string nombre)
        {
            var dt = new DataTable();
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var cmd = new SqlCommand("Exec ConsultarHorario '" + nombre + "'", connect);
            try
            {
                ListaHorarios = new List<Horarios>();
                connect.Open();
                var da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                connect.Close();

                Horarios H = new Horarios();
                //H.id = int.Parse(dt.Rows[0]["id_horario"].ToString());
                H.nombre= dt.Rows[0]["Nombre"].ToString();
                H.dia = dt.Rows[0]["dia"].ToString();
                H.rangohoras = dt.Rows[0]["rangoHoras"].ToString();
                H.ubicacion = dt.Rows[0]["ubicacion"].ToString();
                //H.fk_maestro = int.Parse(dt.Rows[0]["fk_maestro"].ToString());
                ListaHorarios.Add(H);

                return ListaHorarios;
            }
            catch (Exception)
            {
                connect.Close();
                return ListaHorarios;
            }
        }
        //SELECT Id,Nombre FROM Maestros order by Nombre asc;
        public List<Maestros> LlenarDropDownMaestros()
        {
            var dt = new DataTable();
            var connect = new SqlConnection("Server=tcp:tecmaestros.database.windows.net,1433;Initial Catalog=TecMaestros;Persist Security Info=False;User ID=user;Password=T05paTs90@;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            var cmd = new SqlCommand("Exec ConsultarMaestros ", connect);
            try
            {
                ListaMaestros = new List<Maestros>();
                connect.Open();
                var da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                connect.Close();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    maestros = new Maestros();
                    maestros.id = int.Parse(dt.Rows[i]["id_maestro"].ToString());
                    maestros.nombre = dt.Rows[i]["nombre"].ToString();
                    ListaMaestros.Add(maestros);
                }
                return ListaMaestros;
            }
            catch (Exception)
            {
                connect.Close();
                return ListaMaestros;
            }
        }
    }
}
